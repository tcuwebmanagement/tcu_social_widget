<?php
/**
 * Widget to add social media icons
 * to use only with TCU theme installed
 *
 */

// Load the widget
add_action( 'widgets_init', function() {
  register_widget( 'TCU_Social_Widget' );
});

class TCU_Social_Widget extends WP_Widget {

  //Setup widget name, description, ect...
  public function __construct() {
    $widget_options = array(
        'classname' => 'tcu-social-icons-widget',
        'description' => 'Custom Social Icons Widget',
    );
    parent::__construct( 'tcu_social_widget', 'Social Icons', $widget_options );

  }// end class

/**
 * Front-end display of widget
 * @see WP_Widget::widget()
 *
 * @param array $args
 * @param array $instance
 */
public function widget( $args, $instance ) {
  extract( $args );

  // The widget options
    $twitter_url   = $instance['twitter_url'];
    $facebook_url  = $instance['facebook_url'];
    $pinterest_url = $instance['pinterest_url'];
    $youtube_url   = $instance['youtube_url'];
    $flickr_url    = $instance['flickr_url'];
    $instagram_url = $instance['instagram_url'];
    $threads_url = $instance['threads_url'];
    $linkedin_url = $instance['linkedin_url'];

    // Values for SVG Icons
    $svg['twitter']    = "#twitter";
    $svg['facebook']   = "#facebook";
    $svg['pinterest']  = "#pinterest";
    $svg['youtube']    = "#youtube";
    $svg['flickr']     = "#flickr";
    $svg['instagram']  = "#instagram";
    $svg['threads']  = "#threads";
    $svg['linkedin']  = "#linkedin";

    global $post;

    // Before widget (defined by themes)
    echo $before_widget;

   ?>
   <style>
     .tcu-social-icons-widget .social-widgets{
      display: inline-flex;
      justify-content: flex-start;
      list-style: none;
      margin-left: 0;
      margin-right: 0;
    }

    .tcu-social-icons-widget .social-widgets li {
        padding: 0;
        display: inline;
        text-align: left;
    }

    .tcu-social-icons-widget .social-widgets li a > svg {
        height: 30px;
        width: 30px;
    }
   </style>
    <!-- Begin front-end display of list -->
    <ul class="social-widgets cf">

        <?php if($twitter_url) { ?>
            <li>
                <a title="X" href="<?php echo esc_url($twitter_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['twitter']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('Twitter'); ?></span>
                </a>
            </li>
        <?php } ?>

         <?php if($facebook_url) { ?>
            <li>
                <a title="Facebook" href="<?php echo esc_url($facebook_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['facebook']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('Facebook'); ?></span>
                </a>
            </li>
        <?php } ?>

        <?php if($pinterest_url) { ?>
            <li>
                <a title="Pinterest" href="<?php echo esc_url($pinterest_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['pinterest']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('Pinterest'); ?></span>
                </a>
            </li>
        <?php } ?>

        <?php if($youtube_url) { ?>
            <li>
                <a title="YouTube" href="<?php echo esc_url($youtube_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['youtube']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('YouTube'); ?></span>
                </a>
            </li>
        <?php } ?>

        <?php if($flickr_url) { ?>
            <li>
                <a title="Flickr" href="<?php echo esc_url($flickr_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['flickr']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('Flickr'); ?></span>
                </a>
            </li>
        <?php } ?>

        <?php if($instagram_url) { ?>
            <li>
                <a title="Instagram" href="<?php echo esc_url($instagram_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['instagram']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('Instagram'); ?></span>
                </a>
            </li>
        <?php } ?>

        <?php if($threads_url) { ?>
            <li>
                <a title="Threads" href="<?php echo esc_url($threads_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['threads']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('Threads'); ?></span>
                </a>
            </li>
        <?php } ?>

        <?php if($linkedin_url) { ?>
            <li>
                <a title="LinkedIn" href="<?php echo esc_url($linkedin_url); ?>">
                <svg height="30" width="30"><use xlink:href="<?php echo $svg['linkedin']; ?>"></use></svg>
                <span aria-hidden="true" class="tcu-visuallyhidden"><?php _e('LinkedIn'); ?></span>
                </a>
            </li>
        <?php } ?>
    </ul>
    <?php
    echo $after_widget;
  }

/**
 * Back-end display of widget(Form UI)
 *
 * @param array $instance Previously saved values from database
 */
  public function form( $instance ) {

    // check values
    if( $instance ) {
      $twitter_url   = $instance['twitter_url'];
      $facebook_url  = $instance['facebook_url'];
      $pinterest_url = $instance['pinterest_url'];
      $youtube_url   = $instance['youtube_url'];
      $flickr_url    = $instance['flickr_url'];
      $instagram_url = $instance['instagram_url'];
      $threads_url = $instance['threads_url'];
      $linkedin_url = $instance['linkedin_url'];
    } else {
      $twitter_url   = '';
      $facebook_url  = '';
      $pinterest_url = '';
      $youtube_url   = '';
      $flickr_url    = '';
      $instagram_url = '';
      $threads_url = '';
      $linkedin_url = '';
    } ?>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for X:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('twitter_url'); ?>" name="<?php echo $this->get_field_name('twitter_url'); ?>" type="url" value="<?php echo $twitter_url; ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for Facebook:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('facebook_url'); ?>" name="<?php echo $this->get_field_name('facebook_url'); ?>" type="url" value="<?php echo $facebook_url; ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for Pinterest:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('pinterest_url'); ?>" name="<?php echo $this->get_field_name('pinterest_url'); ?>" type="url" value="<?php echo $pinterest_url; ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for Youtube:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('youtube_url'); ?>" name="<?php echo $this->get_field_name('youtube_url'); ?>" type="url" value="<?php echo $youtube_url; ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for Flickr:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('flickr_url'); ?>" name="<?php echo $this->get_field_name('flickr_url'); ?>" type="url" value="<?php echo $flickr_url; ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for Instagram:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('instagram_url'); ?>" name="<?php echo $this->get_field_name('instagram_url'); ?>" type="url" value="<?php echo $instagram_url; ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for Threads:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('threads_url'); ?>" name="<?php echo $this->get_field_name('threads_url'); ?>" type="url" value="<?php echo $threads_url; ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Link for LinkedIn:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('linkedin_url'); ?>" name="<?php echo $this->get_field_name('linkedin_url'); ?>" type="url" value="<?php echo $linkedin_url; ?>">
    </p>

  <?php }

  /**
   * Sanitize widget form values as they are saved
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved
   * @param array $old_instance Previously saved values from database
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {

    $instance = $old_instance;
    $instance['twitter_url']   = strip_tags( $new_instance['twitter_url'] );
    $instance['facebook_url']  = strip_tags( $new_instance['facebook_url'] );
    $instance['pinterest_url'] = strip_tags( $new_instance['pinterest_url'] );
    $instance['youtube_url']   = strip_tags( $new_instance['youtube_url'] );
    $instance['flickr_url']    = strip_tags( $new_instance['flickr_url'] );
    $instance['instagram_url'] = strip_tags( $new_instance['instagram_url'] );
    $instance['threads_url']   = strip_tags( $new_instance['threads_url'] );
    $instance['linkedin_url']   = strip_tags( $new_instance['linkedin_url'] );
    return $instance;
  }
}