<?php
/******************************************************************

Plugin Name:  TCU Social Widget
Description:  This plugin gives you the ability to add a social icons into any widgitized area.
Version:    1.3.0
Author:     Website & Social Media Management
Author URI:   http://mkc.tcu.edu/web-management.asp

License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html

 ******************************************************************/

if ( !defined( 'WPINC' ) ) {
    die;
} // don't remove bracket!


/**
 * Let's get started!
 **/

if( class_exists('TCU_Register_Social_Widget') ) {
    new TCU_Register_Social_Widget;
}

/**
 * Registers plugin and loads functionality
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 *
 **/
class TCU_Register_Social_Widget {

  /**
   * Plugin version
   *
   */
  private static $version = "1.3.0";

  /**
   * Downloads object
   *
   */
  private $widget;

  /**
   * Constructor
   *
   */
  public function __construct() {

    // load all our includes
    $this->load_files();

    // Instatiate TCU_Teaser_Widget
    $this->create_social();

    // Activation and uninstall hooks
    register_activation_hook( __FILE__, array( $this, 'activate_social' ) );
    register_deactivation_hook( __FILE__, array( $this, 'uninstall_social' ) );
  }

  /**
   * Return plugin version
   *
   * @return $version string Plugin version
   */
  public static function get_version() {
    return self::$version;
  }

  /**
   * Instantiate TCU_Social_Widget
   *
   * @return $widget object Accordion Custom Post Type
   **/
  public function create_social() {

    $this->widget = new TCU_Social_Widget;
    return $this->widget;
  }

  /**
   * Check if TCU Web Standards theme is active
   *
   */
  private function check_theme() {

    // Check the current theme
    $theme = wp_get_theme();

    // Check if the current/parent theme is TCU Web Standards
    if( ('TCU Web Standards' == $theme->name) || ('TCU Web Standards' == $theme->parent_theme) ) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Activate plugin
   *
   * @return void
   */
  public function activate_social() {

    // Deactivate plugin if TCU Web Standards Theme is not installed
    if( ! $this->check_theme() ) {

      deactivate_plugins( plugin_basename( __FILE__ ) );
      wp_die( __(sprintf( 'Sorry, but your theme does not support this plugin. Please install the TCU Web Standards Theme.' ) ) );
      return false;
    }

    flush_rewrite_rules();
  }

  /**
   * Uninstall plugin and flush rewrites
   *
   */
  public function uninstall_social() {

    // Flush our rewrites
    flush_rewrite_rules();
  }

  /**
   * Load dependencies
   *
   * @return void
   */
  public function load_files() {

    foreach ( glob( plugin_dir_path( __FILE__ ) . '/classes/*.php' ) as $file ) {
            require_once $file;
    }
  }
}
?>