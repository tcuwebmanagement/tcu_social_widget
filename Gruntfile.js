module.exports = function(grunt) {
    grunt.initConfig({
        // This creates a clean WP plugin copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: ['./*.php', 'classes/*.php', 'css/**.css', 'js/**.js', 'css/!*.map'],
                        dest: 'dist/'
                    }
                ]
            }
        },

        // Let's zip up our /dist (production wordpress plugin)
        // Change version number in tcu_downloads.php
        compress: {
            main: {
                options: {
                    archive: 'tcu_social_widget.1.3.0.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: ['**'],
                        dest: 'tcu_social_widget/'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');

    // Copy to dist/
    grunt.registerTask('default', ['copy']);

    // Zip our clean directory to easily install in WP
    grunt.registerTask('zip', ['compress']);
};
