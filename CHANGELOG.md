## TCU Web Standards Theme

TCU Web Standards theme to be used
for the university. This theme is in line with
the university's brand and web standards.

************************************************

**v1.3.0**

- Add LinkedIn
- Add Threads
- Replace Twitter with X

**v1.2.5**

- Updated the social icons
- Made sure the focus styling wrapped around the icons
- Fixed version number
- Added height and width to svgs

**v1.0.5**

- Added span for missing text in link to solve accessibility.

**v1.0.4**

- Adjusted styles for icons to be inline

**v1.0.3**

- Adjusted inline styles

**v1.0.2**

- Inlined styles
**v1.0.1**

- added CSS to the plugin files rather than theme

**v1.0.0**

- Initial theme